<?php

/**
 * This file is part of reloadAnyResponse plugin
 * Helpers for other plugin : reload or create responses
 * @version 0.2.4
 */

namespace reloadAnyResponse;

use Yii;
use LimeExpressionManager;
use Survey;
use SurveyLanguageSetting;
use SurveyDynamic;

class reloadResponse
{

    /** @var null|integer $surveyId **/
    public $surveyId = null;
    /** @var null|integer $srid **/
    public $srid = null;
    /** @var null|string $language **/
    public $language = null;

    /** @var boolean **/
    private $surveyStarted = false;

    /**
     * @param integer $surveyId
     * @param integer $srid
     * @param string|null $language
     */
    public function __construct($surveyId, $srid, $language = null)
    {
        $this->surveyId = $surveyId;
        $this->srid = $srid;
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$language) {
            $language = Yii::app()->getLanguage();
        }
        if (!$language || !in_array($language, $oSurvey->getAllLanguages())) {
            $language = $oSurvey->language;
        }
        $this->language = $language;
    }

    /**
     * Start and load a Survey
     * No control is done for rights or token in this function
     * @param null|true|integer $step , null : 1st page, true : get current step of response (1st if submitted) , integer : to this step
     * @param boolean|null $resetSubmit, if null : reset according to alloweditaftercompletion
     * @return void
     */
    public function startSurvey($step = true, $resetSubmit = null, $token = '')
    {
        /* If token is different of the current token : need to set it for index */ 
        global $clienttoken;
        $surveyId = $this->surveyId;
        $oResponse = SurveyDynamic::model($surveyId)->findByPk($this->srid);
        if (empty($oResponse)) {
            /* Not available currently, no need */
            throw new \Exception('Reponse not found.');
        }
        $keepMaxStep = Utilities::getSetting($this->surveyId, 'keepMaxStep');
        killSurveySession($surveyId);
        $_SESSION['survey_' . $surveyId]['s_lang'] = $this->language;
        $currentStep = $maxStep = $oResponse->lastpage;
        if ($keepMaxStep) {
            $reloadmaxStep = \reloadAnyResponse\models\responseMaxstep::getMaxstep($surveyId, $this->srid);
            if ($reloadmaxStep && $reloadmaxStep > $currentStep) {
                $maxStep = $reloadmaxStep;
            }
        }

        $submitdate = $oResponse->submitdate;
        LimeExpressionManager::SetSurveyId($surveyId);
        buildsurveysession($surveyId);
        randomizationGroupsAndQuestions($surveyId);
        initFieldArray($surveyId, $_SESSION['survey_' . $surveyId]['fieldmap']);

        if (empty($token) && !empty($oResponse->token)) {
            $token = $_SESSION['survey_' . $surveyId]['token'] = $oResponse->token;
        } else {
            $_SESSION['survey_' . $surveyId]['token'] = $token;
        }
        $_SESSION['survey_' . $surveyId]['srid'] = $oResponse->id;
        /* Warning can be deprecated , updated and removed*/
        loadanswers();
        /* clienttoken is used in index and set in loadanswers */
        if(!empty($clienttoken) && !empty($token)) {
            $_SESSION['survey_' . $surveyId]['token'] = $clienttoken = $token;
        }
        unset($_SESSION['survey_' . $surveyId]['LEMtokenResume']);
        $oSurvey = Survey::model()->findByPk($surveyId);
        switch ($oSurvey->format) {
            case "A":
                $surveyMode = "survey";
                break;
            case "Q":
                $surveyMode = "question";
                break;
            case "G":
            default:
                $surveyMode = "group";
                break;
        }
        $oLanguageSettings = SurveyLanguageSetting::model()->findByPk(array('surveyls_survey_id' => $surveyId,'surveyls_language' => $this->language));
        $radix = getRadixPointData($oLanguageSettings->surveyls_numberformat);
        $radix = $radix['separator'];
        $aSurveyOtions = array(
            'active'                      => $oSurvey->active == 'Y',
            'allowsave'                   => $oSurvey->allowsave == 'Y',
            'anonymized'                  => $oSurvey->anonymized != 'N',
            'assessments'                 => $oSurvey->assessments == 'Y',
            'datestamp'                   => $oSurvey->datestamp == 'Y',
            'deletenonvalues'             => Yii::app()->getConfig('deletenonvalues'),
            'hyperlinkSyntaxHighlighting' => false,
            'ipaddr'                      => $oSurvey->ipaddr == 'Y',
            'radix'                       => $radix,
            'refurl'                      => ($oSurvey->refurl == "Y" && isset($_SESSION["survey_$surveyId"]['refurl'])) ? $_SESSION["survey_$surveyId"]['refurl'] : null,
            'savetimings'                 => $oSurvey->savetimings == "Y",
            'surveyls_dateformat'         => $oLanguageSettings->surveyls_dateformat,
            'startlanguage'               => $this->language,
            'target'                      => Yii::app()->getConfig('uploaddir') . DIRECTORY_SEPARATOR . 'surveys' . DIRECTORY_SEPARATOR . $surveyId . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR,
            'tempdir'                     => Yii::app()->getConfig('tempdir') . DIRECTORY_SEPARATOR,
            'timeadjust'                  => Yii::app()->getConfig("timeadjust"),
            'token'                       => $token,
        );
        LimeExpressionManager::StartSurvey($surveyId, $surveyMode, $aSurveyOtions, false);
        $this->surveyStarted = true;
        if (!empty($oResponse->submitdate)) {
            if ($resetSubmit || (is_null($resetSubmit) && $oSurvey->alloweditaftercompletion != 'Y')) {
                $oResponse->submitdate = null;
                $oResponse->save();
            }
            $maxStep = $_SESSION['survey_' . $surveyId]['totalsteps'];
            $currentStep = 0;
        }
        if (is_null($step)) {
            if ($oSurvey->format == 'A') { /* All in one */
                LimeExpressionManager::JumpTo(1, false, false, true);
            } elseif ($oSurvey->showwelcome != 'Y') { // !$oSurvey->getIsShowWelcome if LS >= 3
                $this->aMoveResult = LimeExpressionManager::NavigateForwards();
            }
            return;
        }
        if (is_int($step)) {
            $currentStep = $step;
        }
        if (!$currentStep) {
            $currentStep = 0;
            if ($oSurvey->showwelcome == 'N') {
                $currentStep = 1;
            }
        }
        $maxStep = max($currentStep, $maxStep);
        \Response::model($this->surveyId)->updateByPk($this->srid, array('submitdate' => null));
        $datestamp = null;
        if (Survey::model()->findByPk($surveyId)->getIsDateStamp()) {
            $datestamp = $oResponse->datestamp;
        }
        LimeExpressionManager::JumpTo($maxStep, false, false);
        if ($maxStep > $currentStep) {
            LimeExpressionManager::JumpTo($currentStep, false, false, true);
        }
        if (Utilities::getSetting($this->surveyId, 'keepMaxStep')) {
            \reloadAnyResponse\models\responseMaxstep::setResponseMaxStep($this->surveyId, $this->srid, $maxStep);
        }
        if (Survey::model()->findByPk($surveyId)->getIsDateStamp() && $datestamp) {
            \Response::model($this->surveyId)->updateByPk($this->srid, array('datestamp' => $datestamp));
        }
        if ($submitdate) {
            \Response::model($this->surveyId)->updateByPk($this->srid, array('submitdate' => $submitdate));
        }
        $_SESSION['survey_' . $surveyId]['LEMtokenResume'] = true;
        $_SESSION['survey_' . $surveyId]['maxstep'] = $maxStep;
        $_SESSION['survey_' . $surveyId]['step'] = $currentStep;
    }
}
