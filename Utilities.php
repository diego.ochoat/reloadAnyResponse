<?php

/**
 * Some Utilities
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2021 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 1.5.3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace reloadAnyResponse;

use App;
use Yii;
use CHttpException;
use Survey;
use SurveyDynamic;
use Token;

class Utilities
{
    /* @var float give information for other plugin of Plugin API version */
    const API = 4.4;

    /* @var array DefaultSettings : the default settings if not set by DB @see reloadAnyResponse::settings */
    const DefaultSettings = array(
        'allowAdminUser' => 1,
        'allowTokenUser' => 1,
        'allowTokenGroupUser' => 1,
        'uniqueCodeAccess' => 1,
        'uniqueCodeCreate' => 0,
        'replaceDefaultSave' => 1,
        'throwErrorRight' => 0,
        'allowUserOnSubmitted' => 1,
        'allowTokenGroupManager' => 0,
        'allowTokenGroupManagerOnSubmitted' => 0,
    );

    /* @var string[] settings that can be set in session to be forced by other plugin */
    const AllowedSessionSettings = array(
        'allowAdminUser',
        'allowTokenUser',
        'allowTokenGroupUser',
        'uniqueCodeAccess',
    );

    /**
     * Create Survey and add current response in $_SESSION after checked permission
     * @param integer $surveydi
     * @param integer $srid
     * @param string $token
     * @param string $accesscode
     * @throws Errors
     * @return void|true : response is reloaded (or is available for reload);
     */
    public static function loadReponse($surveyid, $srid, $token = null, $accesscode = null)
    {
        if (!self::SurveyIsValid($surveyid)) {
            return;
        }
        if (self::getCurrentSrid($surveyid) == $srid && self::getCurrentToken($surveyid, false) == $token) {
            $oStarurl = new StartUrl($surveyid, $token);
            if ($oStarurl->isAvailable()) {
                self::setSaveAutomatic($surveyid);
                return true;
            }
            return;
        }
        $criteria = self::getResponseCriteria($surveyid, $srid);
        $criteria->select = 'id';
        $oResponse = SurveyDynamic::model($surveyid)->find($criteria);
        if (!$oResponse) {
            return self::returnOrThrowException($surveyid, 404, self::translate('Response not found.'));
        }
        $language = App()->getLanguage();
        $oSurvey = \Survey::model()->findByPk($surveyid);
        /* @var boolean, did edition is allowed with current params and settings */
        $editAllowed = false;
        /* @var string : way used for reloading */
        $wayUsed = "";
        /* we check usage by usage : accesscode , token, admin */
        if (
            $accesscode
            && self::getReloadAnyResponseSetting($surveyid, 'uniqueCodeAccess')
        ) {
            $responseLink = \reloadAnyResponse\models\responseLink::model()->findByPk(array('sid' => $surveyid,'srid' => $srid));
            if (!$responseLink) {
                return self::returnOrThrowException($surveyid, 401, self::translate('Sorry, this access code is not valid.'));
            }
            if ($responseLink && $responseLink->accesscode != $accesscode) {
                return self::returnOrThrowException($surveyid, 401, self::translate('Sorry, this access code is not valid.'));
            }
            $editAllowed = true;
            $wayUsed = 'code';
        }
        if (
            !$editAllowed
            && $token
        ) {
            if (!self::TokenAllowEdit($surveyid, $srid, $token)) {
                return self::returnOrThrowException($surveyid, 401, self::translate('Sorry, you don‘t have access to this response.'));
            }
            $editAllowed = true;
            $wayUsed = 'token';
        }
        if (!$editAllowed) {
            $havePermission = self::getReloadAnyResponseSetting($surveyid, 'allowAdminUser') && \Permission::model()->hasSurveyPermission($surveyid, 'responses', 'update');
            if (!$havePermission) {
                return self::returnOrThrowException($surveyid, 401, self::translate('Sorry, you don‘t have access to this response.'));
            }
            $editAllowed = true;
            $wayUsed = 'admin';
        }
        /* $editAllowed is true */
        /* $oResponse is a Response */
        $reloadResponse = new reloadResponse($surveyid, $oResponse->id, $language);
        $reloadResponse->startSurvey(true, false, $token);

        /* Set session for this survey / srid */
        self::setCurrentReloadedWay($surveyid, $wayUsed);
        self::setSaveAutomatic($surveyid);
        self::setCurrentReloadedToken($surveyid, $token);
        self::setCurrentReloadedSrid($surveyid, self::getCurrentSrid($surveyid));
        models\surveySession::saveSessionTime($surveyid, $oResponse->id);
        return true;
    }

    /**
     * Get the search criteria for a specific survey
     * @param integer $survey
     * @param integer $survey
     * @return criteria
     */
    public static function getResponseCriteria($surveyId, $srid)
    {
        /* The filter */
        $criteria = new \CDbcriteria();
        $criteria->compare("id", $srid);
        if (Yii::getPathOfAlias('getQuestionInformation')) {
            $extraFilters = trim(self::getReloadAnyResponseSetting($surveyId, 'extraFilters'));
            if (!empty($extraFilters)) {
                $aColumnToCode = \getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($surveyId);
                $aCodeToColumn = array_flip($aColumnToCode);
                $availableColumns = SurveyDynamic::model($surveyId)->getAttributes();
                $aFieldsLines = preg_split('/\r\n|\r|\n/', $extraFilters, -1, PREG_SPLIT_NO_EMPTY);
                $aFiltersFields = array();
                foreach ($aFieldsLines as $aFieldLine) {
                    if (!strpos($aFieldLine, ":")) {
                        continue; // Invalid line
                    }
                    $key = substr($aFieldLine, 0, strpos($aFieldLine, ":"));
                    $value = substr($aFieldLine, strpos($aFieldLine, ":") + 1);
                    if (array_key_exists($key, $availableColumns)) {
                        $aFiltersFields[$key] = $value;
                    } elseif (isset($aCodeToColumn[$key])) {
                        $aFiltersFields[$aCodeToColumn[$key]] = $value;
                    }
                }
                foreach ($aFiltersFields as $column => $value) {
                    $criteria->compare(App()->getDb()->quoteColumnName($column), $value);
                }
            }
        }
        return $criteria;
    }

    /**
     * get current srid for a survey
     * @param $surveyid integer
     * @return integer|null
     */
    public static function getCurrentSrid($surveyid)
    {
        if (empty($_SESSION['survey_' . $surveyid]['srid'])) {
            return null;
        }
        return $_SESSION['survey_' . $surveyid]['srid'];
    }

    /**
     * get current srid for a survey
     * @param $surveyid integer
     * @return integer|string|null
     */
    public static function getCurrentReloadedSrid($surveyid)
    {
        if (empty($_SESSION['survey_' . $surveyid]['reloadAnyResponseSrid'])) {
            return null;
        }
        return $_SESSION['survey_' . $surveyid]['reloadAnyResponseSrid'];
    }

    /**
     * get current way of current reloaded
     * @param $surveyid integer
     * @return string|null
     */
    public static function getCurrentReloadedWay($surveyid)
    {
        if (empty($_SESSION['survey_' . $surveyid]['reloadAnyResponseWay'])) {
            return null;
        }
        return $_SESSION['survey_' . $surveyid]['reloadAnyResponseWay'];
    }

    /**
     * get current reloaded token for a survey
     * @param $surveyid integer
     * @return string|null
     */
    public static function getCurrentReloadedToken($surveyid)
    {
        if (empty($_SESSION['survey_' . $surveyid]['reloadAnyResponseToken'])) {
            return null;
        }
        return $_SESSION['survey_' . $surveyid]['reloadAnyResponseToken'];
    }

    /**
     * get current token for a survey
     * @param integer $surveyid 
     * @param boolena $byparam
     * @return string|null
     */
    public static function getCurrentToken($surveyid, $byparam = true)
    {
        if($byparam) {
            $tokenParam = App()->getRequest()->getParam('token');
            if ($tokenParam) {
                return $tokenParam;
            }
        }
        if (empty($_SESSION['survey_' . $surveyid]['token'])) {
            return null;
        }
        return $_SESSION['survey_' . $surveyid]['token'];
    }

    /**
     * Set the current token to reloaded token (avoid inavlid token when reload multiple time the same survey)
     * @param $surveyid integer
     * @return void
     */
    public static function fixToken($surveyid)
    {
        if (empty($_SESSION['survey_' . $surveyid]['token'])) {
            return;
        }
        if (empty(self::getCurrentReloadedToken($surveyid))) {
            return;
        }
        $_SESSION['survey_' . $surveyid]['token'] = self::getCurrentReloadedToken($surveyid);
    }

    /**
     * set current srid for a survey
     * @param integer $surveyid
     * @param integer $srid
     * @return void
     */
    public static function setCurrentReloadedSrid($surveyid, $srid)
    {
        $_SESSION['survey_' . $surveyid]['reloadAnyResponseSrid'] = $srid;
    }

    /**
     * set current reloaded way
     * @param integer $surveyid
     * @param string $way
     * @return void
     */
    public static function setCurrentReloadedWay($surveyid, $way)
    {
        $_SESSION['survey_' . $surveyid]['reloadAnyResponseWay'] = $way;
    }

    /**
     * gset current token for a survey
     * @param $surveyid integer
     * @return integer|null
     */
    public static function setCurrentReloadedToken($surveyid, $token)
    {
        if (empty($token)) {
            return;
        }
        $_SESSION['survey_' . $surveyid]['reloadAnyResponseToken'] = $token;
    }

    /**
     * Translate by this plugin
     * @see reloadAnyResponse->_setConfig
     * @param string $string to translate
     * @param string $language for translation
     * @return string
     */
    public static function translate($string, $language = null)
    {
        return Yii::t('', $string, array(), 'ReloadAnyResponseMessages', $language);
    }

    /**
     * Set save automatically
     * @param integer $surveyId
     * @retuirn void
     */
    public static function setSaveAutomatic($surveyid)
    {
        $replaceDefaultSave = self::getReloadAnyResponseSetting($surveyid, 'replaceDefaultSave');
        if (!$replaceDefaultSave) {
            return;
        }
        if ($replaceDefaultSave < 2 && (self::getCurrentReloadedWay($surveyid) == 'admin' || !self::getCurrentReloadedWay($surveyid))) {
            return;
        }
        $_SESSION['survey_' . $surveyid]['scid'] = self::getCurrentSrid($surveyid);
    }

    /**
    * Reset a survey
    * @param integer $surveydi
    * @param integer $srid
    * @param string $token
    * @param boolean $forced
    * @return void
    */
    public static function resetLoadedReponse($surveyid, $srid, $token = null, $forced = false)
    {
        if (self::getCurrentReloadedSrid($surveyid) == $srid) {
            if ($forced || \Survey::model()->findByPk($surveyid)->alloweditaftercompletion != 'Y') {
                $oResponse = \SurveyDynamic::model($surveyid)->updateByPk($srid, array('submitdate' => null));
            }
            if ($token && \Survey::model()->findByPk($surveyid)->anonymized != 'Y') {
                $oResponse = \SurveyDynamic::model($surveyid)->updateByPk($srid, array('token' => $token));
            }
        }
    }

    /**
     * Check if a token is valid with another one
     * @param integer $surveyd
     * @param string $token to be validated
     * @param string $token for control
     * @return boolean
     */
    public static function checkIsValidToken($surveyid, $token, $validtoken = null)
    {
        if (empty($token)) {
            return false;
        }
        if (Survey::model()->findByPk($surveyid)->hasTokensTable) {
            $oToken = Token::model($surveyid)->editable()->findByAttributes(array('token' => $token));
            if (empty($oToken)) {
                return false;
            }
            if (version_compare(App()->getConfig('TokenUsersListAndManageAPI'), '0.14', ">=")) {
                if ($oToken->emailstatus == 'disable') {
                    return false;
                }
            }
        }
        if (empty($validtoken)) {
            return true;
        }
        if ($token == $validtoken) {
            return true;
        }

        if (version_compare(App()->getConfig('TokenUsersListAndManageAPI'), '0.14', ">=")) {
            if (in_array($token, \TokenUsersListAndManagePlugin\Utilities::getTokensList($surveyid, $validtoken, false))) {
                return true;
            }
        } elseif (Yii::getPathOfAlias('responseListAndManage')) {
            if (in_array($token, \responseListAndManage\helpers\tokensList::getTokensList($surveyid, $validtoken))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check minimal possibility to allow reload
     * Without set the osUsrvey (allow to update after
     * @param integer $surveyId
     * @return boolean
     */
    public static function SurveyIsValid($surveyId)
    {
        $now = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", Yii::app()->getConfig("timeadjust"));
        return Survey::model()->count(
            array(
                'condition' => "sid = :sid AND COALESCE(expires, '$now') >= '$now' AND COALESCE(startdate, '$now') <= '$now' AND active ='Y'", 
                'params'=>array(':sid' => $surveyId)
            )
        );
    }

    /**
     * Check if need to throw exception,
     * return if not
     * @param integer $surveyid
     * @params integer error code
     * @param string text for error
     * @throw exception
     * return null;
     */
    private static function returnOrThrowException($surveyid, $code, $text)
    {
        $ThrowException = self::getReloadAnyResponseSetting($surveyid, 'throwErrorRight');
        if ($ThrowException) {
            throw new CHttpException($code, $text);
        }
        \Yii::log($text, 'error', 'plugin.reloadAnyResponse.Exception' . $code);
    }

    /**
     * Get a DB setting from a plugin
     * @see self::getSetting
     * @param integer survey id
     * @param string setting name
     * @return mixed
     */
    public static function getReloadAnyResponseSetting($surveyId, $sSetting)
    {
        return self::getSetting($surveyId, $sSetting);
    }

    /**
     * Get a DB setting from a plugin
     * @param integer survey id
     * @param string setting name
     * @return mixed
     */
    public static function getSetting($surveyId, $sSetting)
    {
        if (!Yii::getPathOfAlias('reloadAnyResponse')) {
            return null;
        }
        /* Session setting (other plugin) */
        if (in_array($sSetting, self::AllowedSessionSettings) && self::getIsForcedAllowedSettings($surveyId, $sSetting)) {
            return true;
        }

        $oPlugin = \Plugin::model()->find(
            "name = :name",
            array(":name" => 'reloadAnyResponse')
        );

        /* This survey setting */
        $oSetting = \PluginSetting::model()->find(
            'plugin_id = :pluginid AND ' . App()->getDb()->quoteColumnName('key') . ' = :key AND model = :model AND model_id = :surveyid',
            array(
                ':pluginid' => $oPlugin->id,
                ':key' => $sSetting,
                ':model' => 'Survey',
                ':surveyid' => $surveyId,
            )
        );
        if (!empty($oSetting)) {
            $value = json_decode($oSetting->value);
            if ($value !== '') {
                return $value;
            }
        }

        /* empty (string) : then get by default setting */
        $oSetting = \PluginSetting::model()->find(
            'plugin_id = :pluginid AND ' . App()->getDb()->quoteColumnName('key') . ' = :key AND model IS NULL',
            array(
                ':pluginid' => $oPlugin->id,
                ':key' => $sSetting,
            )
        );

        if (!empty($oSetting)) {
            $value = json_decode($oSetting->value);
            if ($value !== '') {
                return $value;
            }
        }

        /* empty (string) : then get setting by default */
        if (isset(self::DefaultSettings[$sSetting])) {
            return self::DefaultSettings[$sSetting];
        }

        /* Then finally : null */
        return null;
    }

    /**
     * Set allowed settings by other plugin, replace the current Survey settings.
     * This allow other plugin to force some allowed part, even if it's  not set in plugin setting,
     * Done to allow update reponseListAndManage and questionExtrasurvey without need of modification of settings
     * @param integer survey id
     * @param string setting name
     * @return void
     */
    public static function setForcedAllowedSettings($surveyId, $sSetting)
    {
        if (self::getIsForcedAllowedSettings($surveyId, $sSetting)) {
            return;
        }
        if (!in_array($sSetting, self::AllowedSessionSettings)) {
            throw new CHttpException(500, "Invalid settings to be set : $sSetting");
        }
        $reloadAnyResponseForcedSettings = App()->session['reloadAnyResponseSettings'];
        if (empty($reloadAnyResponseForcedSettings[$surveyId])) {
            $reloadAnyResponseForcedSettings[$surveyId] = array();
        }
        $reloadAnyResponseForcedSettings[$surveyId][$sSetting] = true;
        App()->session['reloadAnyResponseSettings'] = $reloadAnyResponseForcedSettings;
    }

    /**
     * get if a specific setting is allowed
     * @param integer survey id
     * @param string setting name
     * @return boolena
     */
    private static function getIsForcedAllowedSettings($surveyId, $sSetting)
    {
        $reloadAnyResponseForcedSettings = App()->session['reloadAnyResponseSettings'];
        return !empty($reloadAnyResponseForcedSettings[$surveyId][$sSetting]);
    }

    /**
     * Get max step of a response
     * @param integer survey id
     * @param integer survey id
     * @return boolean
     */
    public static function setMaxStep($surveyId, $srid)
    {
        if (!empty($_SESSION['survey_' . $surveyId]['maxstep'])) {
            \reloadAnyResponse\models\responseMaxstep::setResponseMaxStep($surveyId, $srid, $_SESSION['survey_' . $surveyId]['maxstep']);
        }
    }

    /**
     * Check if a response can be loaded by a particular token
     * @param integer $surveyid
     * @rama integer $srid
     * @param string $token
     * @return boolean
     */
    public static function TokenAllowEdit($surveyid, $srid, $token)
    {
        if (Survey::model()->findByPk($surveyid)->getIsAnonymized()) {
            return false;
        }
        $tokenManaged = version_compare(App()->getConfig('TokenUsersListAndManageAPI'), '0.14', ">=");
        if (!self::getReloadAnyResponseSetting($surveyid, 'allowTokenUser') && !$tokenManaged) {
            return false;
        }
        $responseCriteria = self::getResponseCriteria($surveyid, $srid);
        $responseCriteria->select = array('id','submitdate','token');
        $oResponse = SurveyDynamic::model($surveyid)->find($responseCriteria);
        if (empty($oResponse) || empty($oResponse->token)) {
            return false;
        }
        $isValidToken = self::checkIsValidToken($surveyid, $token, $oResponse->token);
        if (!$isValidToken) {
            return false;
        }
        $isManager = false; // If not $tokenManaged : no need to check if manager
        if ($tokenManaged) {
            $tokenTableId = \TokenUsersListAndManagePlugin\Utilities::getSurveyHasTokenTable($surveyid);
            if($tokenTableId) {
                Yii::import('TokenUsersListAndManagePlugin.models.TokenManaged');
                $TokenManaged = \TokenManaged::model($tokenTableId)->findByToken($token);
                if ($TokenManaged && $TokenManaged->getIsManager()) {
                    $isManager = true;
                }
            }
        }
        
        if (!empty($oResponse->submitdate)) {
            $allowSubmitted = self::getReloadAnyResponseSetting($surveyid, 'allowUserOnSubmitted');
            if(!$allowSubmitted) {
                if (!$isManager) {
                    return false;
                }
                if (!self::getReloadAnyResponseSetting($surveyid, 'allowTokenGroupManagerOnSubmitted')) {
                    return false;
                }
            }
        }
        if ($token == $oResponse->token) {
            if (self::getReloadAnyResponseSetting($surveyid, 'allowTokenUser')) {
                return true;
            }
            if (!self::getReloadAnyResponseSetting($surveyid, 'allowTokenGroupManager')) {
                return true;
            }
            return false;
        }

        if (!self::getReloadAnyResponseSetting($surveyid, 'allowTokenGroupUser')) {
            if (!$isManager) {
                return false;
            }
            if (!self::getReloadAnyResponseSetting($surveyid, 'allowTokenGroupManager')) {
                return false;
            }
        }
        if ($tokenManaged) {
            $allowedSridList = \TokenUsersListAndManagePlugin\Utilities::getAllowedSridList($surveyid, $token);
            if (is_array($allowedSridList) && !in_array($oResponse->id, $allowedSridList)) {
                return false;
            }
        }
        return true;
    }
}
