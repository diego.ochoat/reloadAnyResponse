<?php

/**
 * This file is part of reloadAnyResponse plugin
 * @version 0.2.0
 */

namespace reloadAnyResponse\models;

use Yii;
use CActiveRecord;

class responseMaxstep extends CActiveRecord
{
    /**
     * Class reloadAnyResponse\models\responseMaxstep
     *
     * @property integer $sid survey
     * @property integer $srid : response id
     * @property string $maxstep : integer
    */

    /**
     * Set defaults
     * @inheritdoc
     */
    public function init()
    {
        $this->maxstep = 0;
    }

    /** @inheritdoc */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /** @inheritdoc */
    public function tableName()
    {
        return '{{reloadanyresponse_responseMaxstep}}';
    }

    /** @inheritdoc */
    public function primaryKey()
    {
        return array('sid', 'srid');
    }

    /**
     * Return (or create) self
     * @param integer $sid survey
     * @param integer $srid : response id
     * @param string $step : optionnal token, if set : always reset
     * @return int|null current max step , null if error
     */
    public static function setResponseMaxStep($sid, $srid, $step)
    {
        $oResponseMaxstep = self::model()->findByPk(array('sid' => $sid,'srid' => $srid));
        if (!$oResponseMaxstep) {
            $oResponseMaxstep = new self();
            $oResponseMaxstep->sid = $sid;
            $oResponseMaxstep->srid = $srid;
        }
        if ($step < $oResponseMaxstep->maxstep) {
            return $oResponseMaxstep->maxstep;
        }
        $oResponseMaxstep->maxstep = $step;
        if (!$oResponseMaxstep->save()) {
            return null;
        }
        return $oResponseMaxstep->maxstep;
    }

    /**
     * Return the current max step for survey and response
     * @param integer $sid survey
     * @param integer $srid : response id
     * @return int|null current max step , null if not set
     */
    public static function getMaxstep($sid, $srid)
    {
        $oResponseMaxstep = self::model()->findByPk(array('sid' => $sid,'srid' => $srid));
        if (!$oResponseMaxstep) {
            return null;
        }
        return $oResponseMaxstep->maxstep;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $aRules = array(
            array('sid', 'required'),
            array('srid', 'required'),
            array('maxstep', 'numerical', 'integerOnly' => true),
            array('sid,srid', 'numerical', 'integerOnly' => true),
            array('srid', 'unique', 'criteria' => array(
                    'condition' => 'sid=:sid',
                    'params' => array(':sid' => $this->sid)
                ),
                'message' => sprintf("Srid : '%s' already set for '%s'.", $this->srid, $this->sid),
            ),
        );
        return $aRules;
    }
}
