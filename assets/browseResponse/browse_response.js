$(document).ajaxComplete(function() {
    if($('#responses-grid table').data('reloadAnyResponse') == 'done') {
        return;
    }
    $('#responses-grid table tbody').find('tr').each(function( index, element) {
        var pencil = $(element).find('.button-column').find('a .fa-pencil');
        if($(pencil).length) {
            var srid = $(element).find('input').eq(0).val();
            var newLink = $('<a/>', {
                'class': 'btn btn-default btn-xs',
                'href': LS.reloadAnyResponse.baseUrl + srid,
                'target': '_blank',
                'title': LS.reloadAnyResponse.lang.launch,
                'data-toggle': 'tooltip'
            }).html('<i class="fa fa-pencil-square-o text-success" aria-hidden="true"></i>'); 
            $(pencil).closest("a").after(newLink);
        }
    });
    $('#responses-grid table').data('reloadAnyResponse','done');
});
