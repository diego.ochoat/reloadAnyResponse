<?php

/**
 * Plugin helper for limesurvey : new class and function allowing to reload any survey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2018-2021 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 4.4.4
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class reloadAnyResponse extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $description = 'New class and function allowing to reload any survey.';
    protected static $name = 'reloadAnyResponse';

    protected static $dbVersion = 5;

    /* @var null|interger Keep reload srid during all events */
    private $reloadedSrid = null;
    /* @var null|string Keep reload token during all events */
    private $reloadedToken = null;

    /* @var null|interger Keep current surveyid */
    private $surveyId = null;

    /* @var boolean need to fix alloweditaftercompletion */
    private $enablealloweditaftercompletion = false;
    /* @var boolean need to fix tokenanswerspersistence */
    private $disabletokenanswerspersistence = false;

    /**
     * @var array[] the settings
     */
    protected $settings = array(
        'information' => array(
            'type' => 'info',
            'content' => 'The default settings for all surveys. Remind no system is set in this plugin to show or send link to user.',
        ),
        'allowAdminUser' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Allow admin user to reload any survey with response id.",
            'default' => 1,
        ),
        'replaceEditResponse' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Replace edit response in browse response interface.",
            'help' => "When an admin user want to edit an existing response directly redirect to editing the public survey.",
            'default' => 0,
        ),
        'addEditResponseOnBrowse' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Add an edit button on browser.",
            'help' => "Add a button on browse reponse if user is allowed to edit (only if replace edit response is not activated).",
            'default' => 1,
        ),
        'allowTokenUser' => array(
            'type' => 'select',
            'options' => array(
                1 => "Yes",
                0 => "No",
            ),
            'label' => "Allow user with a valid token.",
            'default' => 1,
        ),
        'allowTokenGroupUser' => array(
            'type' => 'select',
            'options' => array(
                1 => "Yes",
                0 => "No",
            ),
            'label' => "Allow user with a token in same group.",
            'help' => "Group is related to User group management from responseListAndManage plugin",
            'default' => 1,
        ),
        'uniqueCodeCreate' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Create automatically unique code for all surveys.",
            'help' => "If code exist, it can be always used.",
            'default' => 0,
        ),
        'uniqueCodeAccess' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Allow entering unique code for all surveys if exist.",
            'help' => "If you set to no, this disable usage for other plugins.",
            'default' => 1,
        ),
        'deleteLinkWhenResponseDeleted' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Delete the link of response when a response is deleted.",
            'help' => "This delete the response link when response is deleted. If you use VV import, don't activate this option.",
            'default' => 0,
        ),
        'deleteLinkWhenSurveyDeactivated' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Delete the link of all responses of a survey when it's deactivated.",
            'help' => "Since response table keep the current auto increment value, leave this option can not broke response security.",
            'default' => 0,
        ),
        'deleteLinkWhenSurveyDeleted' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Delete the link of all responses of a survey when it's deleted.",
            'help' => "To avoid a big table.",
            'default' => 1,
        ),
        'disableMultiAccess' => array(
            'type' => 'info',
            'content' => "<div class='alert alert-info'>You need renderMessage for disabling multiple access.</div>",
            'default' => 1,
        ),
        'multiAccessTime' => array(
            'type' => 'int',
            'label' => 'Time for disable multiple access (in minutes) (config.php settings replace it of not exist)',
            'help' => 'Before save value or entering survey : test if someone else edit response in this last minutes. Disable save and show a message if yes. Set to empty disable system but then answer of user can be deleted by another user without any information…',
            'htmlOptions' => array(
                'min' => 1,
                'placeholder' => 'Disable',
            ),
            'default' => '',
        ),
        'throwErrorRight' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => 'Throw error when try to edit a response without right (Default)',
            'help' => 'Send an http 401 error when srid is in url, but user did ,not have right. Else create a new response (according to survey settings) . ',
            'default' => 0,
        ),
        'replaceDefaultSave' => array(
            'type' => 'select',
            'options' => array(
                0 => "Never",
                1 => "When survey is reloaded by access code or token.",
                2 => "Always, each time survey is reloaded.",
            ),
            'label' => 'Replace the default LimeSurvey system if survey reloaded',
            'help' => 'When participant try to save a reloaded reponse : it was directly saved without showing the save form.',
            'default' => 1,
        ),
        'clearAllAction' => array(
            'type' => 'select',
            'options' => array(
                'reset' => "Reset session, don‘t delete current response",
                'partial' => "Reset session, delete current response if was not submitted.",
                'all' => "Reset session, delete current response in any condition.",
            ),
            'label' => "Action when using clearall action.",
            'help' => "Action to do when participant want to clear all. Default action by LimeSurvey was to delete not submitted response.",
            'default' => 'partial',
        ),
        'clearAllActionForced' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Replace clearall action even if response was not reloaded",
            'default' => 0,
        ),
        'reloadResetSubmitted' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "When survey is save or move for the 1st time : reset it as not submitted.",
            'default' => 1,
        ),
        'keepMaxStep' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Keep max step during each load and save.",
            'help' => "Warning : when set this by default : it update all current survey table",
            'default' => 0,
        ),
        /* surveySession settings */
        'noHttpUserAgent' => array(
            'type' => 'select',
            'options' => array(
                'forbidden' => 'Disable access with a 403',
                'action' => 'Allow access and block other access',
            ),
            'htmlOptions' => array(
                'empty' => 'Allow access but don‘t block other access',
            ),
            'label' => 'Action to do without HTTP_USER_AGENT',
            'help' => 'By default : show the page but don\'t disable other access. You can choose to send a http 403 error (Forbidden) . ',
            'default' => '',
        ),
        'botRegexp' => array(
            'type' => 'string',
            'htmlOptions' => array(
                'placeholder' => '/bot|crawl|slurp|spider|mediapartners|lua-resty-http/i',
            ),
            'label' => 'Bot regexp',
            'help' => 'Usage of preg_match : you can add new bot or invalid request with “|newbot”.',
            'default' => '',
        ),
        'botHttpUserAgent' => array(
            'type' => 'select',
            'options' => array(
                'forbidden' => 'Disable access with a 403',
                'action' => 'Allow access and block other access',
            ),
            'htmlOptions' => array(
                'empty' => 'Allow access but don‘t block other access',
            ),
            'label' => 'Action to do with bot',
            'help' => 'By default : show the page but don\'t disable other access. You can choose to send a http 403 error (Forbidden) . ',
            'default' => '',
        ),
    );

    /** @inheritdoc **/
    public function init()
    {
        /* Set path of Alias at start */
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        /* Set the config */
        $this->subscribe('afterPluginLoad', 'setConfigInAfterPluginLoad');

        if (Yii::app() instanceof CConsoleApplication) {
            return;
        }

        /* Managing unique code for Response and SurveyDynamic */
        $this->subscribe('afterModelSave');
        $this->subscribe('afterModelDelete');
        /* Delete related link for Survey */
        $this->subscribe('afterSurveySave');
        $this->subscribe('afterSurveyDelete');
        $this->subscribe('beforeSurveyDeleteMany');

        /* Get the survey by srid and code */
        /* Save current session */
        $this->subscribe('beforeSurveyPage');
        /* Some needed action when srid is set */
        $this->subscribe('getPluginTwigPath');
        /* Replace existing system if srid = new */
        $this->subscribe('beforeLoadResponse');
        /* Add a checker when multiple tab is open */
        $this->subscribe('beforeQuestionRender');
        /* Survey settings */
        $this->subscribe('beforeSurveySettings');
        $this->subscribe('newSurveySettings');

        /* The survey setting in TULAM */
        $this->subscribe('beforeTULAMSurveySettings');
        $this->subscribe('newTULAMSurveySettings');

        /* delete current session*/
        $this->subscribe("afterSurveyComplete", 'afterSurveyDeleteSurveySession');
        $this->subscribe("afterSurveyQuota", 'afterSurveyDeleteSurveySession');
        /* delete current session when unload */
        $this->subscribe("newDirectRequest", 'newDirectRequest');
        /* redirect to editing survey */
        $this->subscribe("beforeControllerAction", 'beforeControllerAction');

        /* Logout remove all session, survey too, then must delete current related surveySessionId */
        $this->subscribe("beforeLogout", "deleteAllBySessionId");
    }

    /**
    * Delete all related current survey session from this user
    * @return @void
    */
    public function deleteAllBySessionId()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        \reloadAnyResponse\models\surveySession::deleteAllBySessionId();
    }

    /** @inheritdoc **/
    public function getPluginSettings($getValues = true)
    {
        if (!Permission::model()->hasGlobalPermission('settings', 'read')) {
            throw new CHttpException(403);
        }
        /* @todo translation of label and help */
        $pluginSetting = parent::getPluginSettings($getValues);
        return $pluginSetting;
    }
    /** @inheritdoc **/
    public function saveSettings($settings)
    {
        if (!Permission::model()->hasGlobalPermission('settings', 'update')) {
            throw new CHttpException(403);
        }
        parent::saveSettings($settings);
    }

    /** @inheritdoc **/
    public function beforeSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oEvent = $this->event;
        $oSurvey = Survey::model()->findByPk($oEvent->get('survey'));
        /* currentDefault translation */
        $allowAdminUserDefault = $this->get('allowAdminUser', null, null, $this->settings['allowAdminUser']['default']) ? gT('Yes') : gT('No');
        $allowTokenDefault = $this->get('allowTokenUser', null, null, $this->settings['allowTokenUser']['default']) ? gT('Yes') : gT('No');
        $allowTokenGroupUserDefault = $this->get('allowTokenGroupUser', null, null, $this->settings['allowTokenGroupUser']['default']) ? gT('Yes') : gT('No');
        $uniqueCodeCreateDefault = $this->get('uniqueCodeCreate', null, null, $this->settings['uniqueCodeCreate']['default']) ? gT('Yes') : gT('No');
        $uniqueCodeAccessDefault = $this->get('uniqueCodeAccess', null, null, $this->settings['uniqueCodeAccess']['default']) ? gT('Yes') : gT('No');
        $throwErrorRightDefault = $this->get('throwErrorRight', null, null, $this->settings['throwErrorRight']['default']) ? gT('Yes') : gT('No');
        $replaceDefaultSaveDefault = $this->get('replaceDefaultSave', null, null, $this->settings['replaceDefaultSave']['default']);
        $clearAllActionDefault = $this->get('clearAllAction', null, null, $this->settings['clearAllAction']['default']);
        $reloadResetSubmittedDefault = $this->get('reloadResetSubmitted', null, null, $this->settings['reloadResetSubmitted']['default']) ? gT('Yes') : gT('No');
        $keepMaxStepDefault = $this->get('keepMaxStep', null, null, $this->settings['keepMaxStep']['default']) ? gT('Yes') : gT('No');

        switch ($clearAllActionDefault) {
            case 'reset':
                $clearAllActionDefault = $this->translate("Do not delete");
                break;
            case 'all':
                $clearAllActionDefault = $this->translate("Always delete");
                break;
            case 'partial':
                $clearAllActionDefault = $this->translate("Delete not submitted");
                break;
            default:
                /* Must not come here, show the current value */
        }

        switch ($replaceDefaultSaveDefault) {
            case 0:
                $replaceDefaultSaveDefault = $this->translate("never");
                break;
            case 1:
            default:
                $replaceDefaultSaveDefault = $this->translate("with access code or token");
                break;
            case 2:
                $replaceDefaultSaveDefault = $this->translate("always");
                break;
        }

        $clearAllActionForcedDefault =  $this->get('clearAllActionForced', null, null, $this->settings['clearAllActionForced']['default']) ? gT('Yes') : gT('No');
        $multiAccessTimeDefault = $this->get('multiAccessTime', null, null, $this->settings['multiAccessTime']['default']) ? $this->get('multiAccessTime', null, null, $this->settings['multiAccessTime']['default']) : gT('Disable');

        /* Helper */
        $allowTokenUserHelp = $this->getTokenUserhelp($oSurvey);

        $oEvent->set("surveysettings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array(
                'allowAdminUser' => array(
                    'type' => 'select',
                    'label' => $this->translate("Allow admin user to reload any response with response id."),
                    'options' => array(
                        1 => gT("Yes"),
                        0 => gT("No"),
                    ),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $allowAdminUserDefault)),
                    ),
                    'current' => $this->get('allowAdminUser', 'Survey', $oEvent->get('survey'), "")
                ),
                'allowTokenUser' => array(
                    'type' => 'select',
                    'label' => $this->translate("Allow participant with token to reload or create responses."),
                    'help' =>  $allowTokenUserHelp,
                    'options' => array(
                        1 => gT("Yes"),
                        0 => gT("No"),
                    ),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $allowTokenDefault)),
                    ),
                    'current' => $this->get('allowTokenUser', 'Survey', $oEvent->get('survey'), "")
                ),
                'allowTokenGroupUser' => array(
                    'type' => 'select',
                    'label' => $this->translate("Allow participant with token in same group to reload responses."),
                    'help' => $this->translate("Related to group of user user by TokenUsersListAndManage or responseListAndManage plugin. User must be alowed to reload own reponse."),
                    'options' => array(
                        1 => gT("Yes"),
                        0 => gT("No"),
                        ),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $allowTokenGroupUserDefault)),
                    ),
                    'current' => $this->get('allowTokenGroupUser', 'Survey', $oEvent->get('survey'), "")
                ),
                'uniqueCodeCreate' => array(
                    'type' => 'select',
                    'label' => $this->translate("Create unique code automatically."),
                    'options' => array(
                        1 => gT("Yes"),
                        0 => gT("No"),
                    ),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $uniqueCodeCreateDefault)),
                    ),
                    'current' => $this->get('uniqueCodeCreate', 'Survey', $oEvent->get('survey'), "")
                ),
                'uniqueCodeAccess' => array(
                    'type' => 'select',
                    'label' => $this->translate("Allow using unique code if exist."),
                    'options' => array(
                        1 => gT("Yes"),
                        0 => gT("No"),
                    ),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $uniqueCodeAccessDefault)),
                    ),
                    'current' => $this->get('uniqueCodeAccess', 'Survey', $oEvent->get('survey'), ""),
                ),
                /* Specific disable */
                'extraFilters' => array(
                    'type' => 'text',
                    'label' => $this->translate("Extra condition for allowing edition."),
                    'default' => '',
                    'current' => $this->get('extraFilters', 'Survey', $oEvent->get('survey'), ""),
                    'help' => $this->translate('One field by line, field must be a valid question code (single question only). Field and value are separated by colon (<code>:</code>) . ')
                        . "<br>" . $this->translate('If user try to edit an unavailable response, a 404 error is shown.'),
                ),
                'multiAccessTime' => array(
                    'type' => 'int',
                    'label' => $this->translate("Time for disable multiple access (in minutes) . "),
                    'help' => $this->translate("Set to 0 to disable, leave empty for default. If disable : same response can be open at same time; last saved response overwrites the previous one."),
                    'htmlOptions' => array(
                        'min' => 0,
                        'placeholder' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $multiAccessTimeDefault)),
                    ),
                    'current' => $this->get('multiAccessTime', 'Survey', $oEvent->get('survey'), "")
                ),
                'throwErrorRight' => array(
                    'type' => 'select',
                    'options' => array(
                        1 => gT("Yes"),
                        0 => gT("No"),
                    ),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $throwErrorRightDefault)),
                    ),
                    'label' => $this->translate("Throw a 401 error if try to reload without rights."),
                    'help' => $this->translate("Send an http 401 error when srid is in url, but user did ,not have right. Else create a new response (according to survey settings)"),
                    'current' => $this->get('throwErrorRight', 'Survey', $oEvent->get('survey'), "")
                ),
                /* Replacing save */
                'replaceDefaultSave' => array(
                    'type' => 'select',
                    'options' => array(
                        0 => gT("Never"),
                        1 => gT("If reloaded with access code or token"),
                        2 => gT("Always (if reloaded)"),
                    ),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $replaceDefaultSaveDefault)),
                    ),
                    'label' => $this->translate("Save reloaded response transparently."),
                    'help' => $this->translate("Replace the LimeSurvey save form : directly save the current reponse when user click on save all. With reloaded only : tyhis allow admin user to create easily save action."),
                    'current' => $this->get('replaceDefaultSave', 'Survey', $oEvent->get('survey'), "")
                ),
                /* Clear all action */
                'clearAllAction' => array(
                    'type' => 'select',
                    'options' => array(
                        'reset' => gT("Reset session, don‘t delete current response"),
                        'partial' => gT("Reset session, delete current response if was not submitted."),
                        'all' => gT("Reset session, delete current response in any condition."),
                    ),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $clearAllActionDefault)),
                    ),
                    'label' => $this->translate("Action when using clearall action."),
                    'help' => $this->translate("Action to do when participant want to clear all. Default action by LimeSurvey was to delete not submitted answer."),
                    'current' => $this->get('clearAllAction', 'Survey', $oEvent->get('survey'), "")
                ),
                'clearAllActionForced' => array(
                    'type' => 'select',
                    'options' => array(
                        1 => gT("Yes"),
                        0 => gT("No"),
                    ),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $clearAllActionForcedDefault)),
                    ),
                    'label' => $this->translate("Replace clearall action even if response is not reloaded."),
                    'current' => $this->get('clearAllActionForced', 'Survey', $oEvent->get('survey'), "")
                ),
                /* Reset to not submitted when open */
                'reloadResetSubmitted' => array(
                    'type' => 'select',
                    'options' => array(
                        1 => gT("Yes"),
                        0 => gT("No"),
                    ),
                    'label' => $this->translate("Reset submitdate on the first action done."),
                    'help' => $this->translate("When a submitted survey is open without action : nothing was updated, if user do an action : you can choose to reset the respnse to not submitted."),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $reloadResetSubmittedDefault)),
                    ),
                    'current' => $this->get('reloadResetSubmitted', 'Survey', $oEvent->get('survey'), "")
                ),
                /* Save (abnd load) max step */
                'keepMaxStep' => array(
                'type' => 'select',
                'options' => array(
                    1 => gT("Yes"),
                    0 => gT("No"),
                ),
                'label' => $this->translate("Keep max page seen when load and save."),
                'help' => $this->translate("Add a column in database to keep max page seen. Reload it when load response."),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $keepMaxStepDefault)),
                ),
                'current' => $this->get('reloadResetSubmitted', 'Survey', $oEvent->get('survey'), "")
                ),
            ),
        ));
    }

    /** @inheritdoc
     * Add some specific settings in TUMAL
     **/
    public function beforeTULAMSurveySettings()
    {
        $SurveySettingsEvent = $this->getEvent();
        $surveyId = $SurveySettingsEvent->get('survey');
        $oSurvey = Survey::model()->findByPk($surveyId);
        $allowTokenDefault = $this->get('allowTokenUser', null, null, $this->settings['allowTokenUser']['default']) ? gT('Yes') : gT('No');
        $allowTokenGroupUserDefault = $this->get('allowTokenGroupUser', null, null, $this->settings['allowTokenGroupUser']['default']) ? gT('Yes') : gT('No');
        $allowTokenUserHelp = $this->getTokenUserhelp($oSurvey);
        $SurveySettingsEvent->set("settings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array(
                'allowTokenUser' => array(
                    'type' => 'select',
                    'label' => $this->translate("Allow participant with token to reload or create responses."),
                    'help' =>  $allowTokenUserHelp,
                    'options' => array(
                        1 => gT("Yes"),
                        0 => gT("No"),
                    ),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $allowTokenDefault)),
                    ),
                    'current' => $this->get('allowTokenUser', 'Survey', $surveyId, "")
                ),
                'allowTokenGroupUser' => array(
                    'type' => 'select',
                    'label' => $this->translate("Allow participant with token in same group to reload responses."),
                    'help' => $this->translate("User must be allowed to reload own reponse. If you use another plugin, like questionExtraSurvey : you don't need to set it here."),
                    'options' => array(
                        1 => gT("Yes"),
                        0 => gT("No"),
                    ),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $allowTokenGroupUserDefault)),
                    ),
                    'current' => $this->get('allowTokenGroupUser', 'Survey', $surveyId, "")
                ),
                'allowUserOnSubmitted' => array(
                    'type' => 'select',
                    'label' => $this->translate("On submitted response."),
                    'help' => $this->translate("If user is allowed to edit response : allowed to edit submitted response too. This was checked even if another plugin allow edition."),
                    'options' => array(
                        0 => gT("No"),
                    ),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode($this->translate("Not restricted")),
                    ),
                    'current' => $this->get('allowUserOnSubmitted', 'Survey', $surveyId, "")
                ),
                'allowTokenGroupManager' => array(
                    'type' => 'select',
                    'label' => $this->translate("Allow manager of group to reload response"),
                    'help' => $this->translate("Checked only if user on same group didn't have this right."),
                    'options' => array(
                        1 => gT("Yes"),
                    ),
                    'htmlOptions' => array(
                        'empty' => $this->translate("Follow user right."),
                    ),
                    'current' => $this->get('allowManagerGroupUser', 'Survey', $surveyId, "")
                ),
                'allowTokenGroupManagerOnSubmitted' => array(
                    'type' => 'select',
                    'label' => $this->translate("Allow manager of group to reload submitted response"),
                    'help' => $this->translate("Checked only if user on same group didn't have this right."),
                    'options' => array(
                        1 => gT("Yes"),
                    ),
                    'htmlOptions' => array(
                        'empty' => $this->translate("Follow user right."),
                    ),
                    'current' => $this->get('allowTokenGroupManagerOnSubmitted', 'Survey', $surveyId, "")
                ),
            )
        ));
    }

    /**
     * See event
     * Save the value set in settings
     * return void
     */
    public function newTULAMSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $SurveySettingsEvent = $this->getEvent();
        $surveyId = $SurveySettingsEvent->get('survey');
        $settings = $SurveySettingsEvent->get('settings');
        foreach($settings as $setting => $value) {
            $this->set($setting, $value, 'Survey', $surveyId);
        }
    }

    /**
     * When need to do something
     */
    public function beforeControllerAction()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getEvent()->get("controller") == "survey" && $this->getEvent()->get("action") == "index") {
            $this->fixSurveyAttributes();
            return;
        }
        if ($this->getEvent()->get("controller") != "admin") {
            return;
        }
        if ($this->getEvent()->get("action") != "responses" && $this->getEvent()->get("action") != "dataentry") {
            return;
        }
        $surveyid = App()->getRequest()->getParam('surveyid');
        if (!Permission::model()->hasSurveyPermission($surveyid, 'response', 'update')) {
            return;
        }
        if (!\reloadAnyResponse\Utilities::SurveyIsValid($surveyid)) {
            return;
        }
        if($this->get('addEditResponseOnBrowse',null, null, 1) && !$this->get('replaceEditResponse',null,null,0)) {
            $reloadAnyResponseOptions = array(
                'baseUrl' => Yii::app()->createUrl("survey/index", array('sid' => $surveyid,'newtest'=>'Y','srid'=>"")),
                'lang' => array(
                    'launch' => gT("Execute Survey"),
                ),
            );
            App()->clientScript->registerScript(
                'reloadAnyResponseAdminScript',
                'LS.reloadAnyResponse = ' . json_encode($reloadAnyResponseOptions),
                CClientScript::POS_BEGIN
            );
            App()->clientScript->registerScriptFile(
                App()->assetManager->publish(dirname(__FILE__) . '/assets/browseResponse/browse_response.js'),
                CClientScript::POS_END
            );
            return;
        }
        if(!$this->get('replaceEditResponse',null,null,0)) {
            return;
        }
        if ($this->getEvent()->get("action") != "dataentry") {
            return;
        }
        $criteria = \reloadAnyResponse\Utilities::getResponseCriteria($surveyid, $srid);
        $oResponse = Response::model($surveyid)->find($criteria);
        if (empty($oResponse)) {
            return;
        }
        $surveyLink = Yii::app()->createUrl("survey/index", array('sid' => $surveyid,'srid' => $srid,'newtest' => 'Y'));
        $this->getEvent()->set('run', false);
        App()->getController()->redirect($surveyLink);
    }

    /**
     * Check if survey seetings need to be updated with current params
     * @return void
     */
    private function fixSurveyAttributes()
    {
        $sid = App()->getRequest()->getParam('sid',
            App()->getRequest()->getParam('surveyid')
        );
        $srid = App()->getRequest()->getParam('srid');
        if (empty($srid)) {
            $srid = \reloadAnyResponse\Utilities::getCurrentSrid($sid);
        }
        if (empty($srid)) {
            return;
        }
        if(Permission::model()->hasSurveyPermission($sid, 'responses', 'update')) {
            $startUrl = new \reloadAnyResponse\StartUrl($sid);
            if ($startUrl->isAvailable()) {
                $this->enablealloweditaftercompletion = true;
                Survey::model()->resetCache();
                $this->subscribe('afterFindSurvey', 'setSurveyAttributes');
                return;
            }
        }
        $token = App()->getRequest()->getParam('token');
        if (empty($token)) {
            $token = \reloadAnyResponse\Utilities::getCurrentReloadedToken($sid);
        }
        
        if(!empty($token)) {
            if ($srid == "new") {
                /* @todo : allow create system and checker */
                if(\reloadAnyResponse\Utilities::getReloadAnyResponseSetting($sid, 'allowTokenUser')) {
                    $this->disabletokenanswerspersistence = true;
                    $this->enablealloweditaftercompletion = true;
                    Survey::model()->resetCache();
                    $this->subscribe('afterFindSurvey', 'setSurveyAttributes');
                }
                return;
            }
            $allowEdit = \reloadAnyResponse\Utilities::TokenAllowEdit($sid, $srid, $token);
            if (!$allowEdit) {
                return;
            }
            $this->enablealloweditaftercompletion = true;
            Survey::model()->resetCache();
            $this->subscribe('afterFindSurvey', 'setSurveyAttributes');
            return;
        }
        if(!App()->getRequest()->getParam('code')) {
            return;
        }
        /* Here with accesscode only : review system */
        $startUrl = new \reloadAnyResponse\StartUrl($sid);
        if (!$startUrl->isAvailable()) {
            return;
        }
        $this->enablealloweditaftercompletion = true;
        Survey::model()->resetCache();
        $this->subscribe('afterFindSurvey', 'setSurveyAttributes');
    }

    /**
     * Set a surey as editable
     * @see afterFindSurvey event
     */
    public function setSurveyAttributes()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->enablealloweditaftercompletion) {
            $this->getEvent()->set('alloweditaftercompletion', 'Y');
        }
        if ($this->disabletokenanswerspersistence) {
            $this->getEvent()->set('tokenanswerspersistence', '');
        }
    }

    /** @inheritdoc **/
    public function newSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->event;
        foreach ($event->get('settings') as $name => $value) {
            $this->set($name, $value, 'Survey', $event->get('survey'));
        }
    }

    /** @inheritdoc
     * Delete all response link when survey is set to active != Y
     **/
    public function afterSurveySave()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getCurrentSetting('deleteLinkWhenSurveyDeactivated')) {
            $oSurvey = $this->getEvent()->get('model');
            if ($oSurvey->sid && $oSurvey->active != 'Y') {
                $deleted = \reloadAnyResponse\models\responseLink::model()->deleteAll("sid = :sid", array(':sid' => $oSurvey->sid));
                if ($deleted > 0) { // Don't log each time, can be saved for something other …
                    $this->log(sprintf("%d responseLink deleted for %d", $deleted, $oSurvey->sid), CLogger::LEVEL_INFO);
                }
            }
        }
    }

    /** @inheritdoc
     * Delete all response link when survey is deleted
     **/
    public function afterSurveyDelete()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getCurrentSetting('deleteLinkWhenSurveyDeleted')) {
            $oSurvey = $this->getEvent()->get('model');
            if ($oSurvey->sid) {
                $deleted = \reloadAnyResponse\models\responseLink::model()->deleteAll("sid = :sid", array(':sid' => $oSurvey->sid));
                if ($deleted > 0) { // Don't log each time, can be saved for something other …
                    $this->log(sprintf("%d responseLink deleted for %d", $deleted, $oSurvey->sid), CLogger::LEVEL_INFO);
                }
            }
        }
    }

    /** @inheritdoc
     * Delete all response link when surveys is deleted
     * @todo
     **/
    public function beforeSurveyDeleteMany()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getCurrentSetting('deleteLinkWhenSurveyDeleted')) {
            $criteria = $this->getEvent()->get('filterCriteria');
        }
    }

    /** @inheritdoc
     * Create the response link when survey is started
     * Remind : it's better if your plugin create this link directly `$responseLink = \reloadAnyResponse\models\responseLink::setResponseLink($iSurvey,$iResponse,$token);`
     * Before 3.15.0 : afterResponseSave event didn't exist
     **/
    public function afterModelSave()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oModel = $this->getEvent()->get('model');
        $className = get_class($oModel);
        /* Create responlink for survey and srid (work when start a survey) */
        if ($className == 'SurveyDynamic' || $className == 'Response') {
            $sid = str_replace(array('{{survey_','}}'), array('',''), $oModel->tableName());
            /* Test for sid activation */
            if (!$this->getIsActivated('uniqueCodeCreate', $sid)) {
                return;
            }
            $srid = isset($oModel->id) ? $oModel->id : null;
            if ($sid && $srid) {
                $responseLink = \reloadAnyResponse\models\responseLink::model()->findByPk(array('sid' => $sid,'srid' => $srid));
                /* @todo : add a way to reset potential token ?
                 * @see https://gitlab.com/SondagesPro/managament/responseListAndManage/blob/80fb8571d394eedda6abfbfd1757c5322f699608/responseListAndManage.php#L2336
                 **/
                if (!$responseLink) {
                    $token = isset($oModel->token) ? $oModel->token : null;
                    $responseLink = \reloadAnyResponse\models\responseLink::setResponseLink($sid, $srid, $token);
                    if (!$responseLink) {
                        $this->log("Unable to save responseLink with following errors.", CLogger::LEVEL_ERROR);
                        $this->log(CVarDumper::dumpAsString($responseLink->getErrors()), CLogger::LEVEL_ERROR);
                    }
                }
            }
        }
    }

    /**
     * @inheritdoc
     * Delete related responseLink when a response is deleted
     * Before 3.15.0 : afterResponseSave event didn't exist
     * This function is in testing currently
     **/
    public function afterModelDelete()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getCurrentSetting('deleteLinkWhenResponseDeleted')) {
            $oModel = $this->getEvent()->get('model');
            $className = get_class($oModel);
            if ($className == 'SurveyDynamic' || $className == 'Response') {
                $sid = str_replace(array('{{survey_','}}'), array('',''), $oModel->tableName());
                $srid = isset($oModel->id) ? $oModel->id : null;
                if ($srid) {
                    \reloadAnyResponse\models\responseLink::model()->deleteByPk(array('sid' => $sid,'srid' => $srid));
                }
            }
        }
    }

    /** @See event */
    public function beforeLoadResponse()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $srid = App()->getRequest()->getQuery('srid');
        $surveyId = $this->getEvent()->get('surveyId');
        $oSurvey = Survey::model()->findByPk($surveyId);
        $oResponses = $this->getEvent()->get('responses');
        $oResponse = $this->getEvent()->get('response');/* Get the current response set */
        $token = App()->getRequest()->getParam('token');

        /* control multi access to token with token and allow edit reponse */
        $oSurvey = Survey::model()->findByPk($surveyId);
        if ($srid == 'new' && $token && $this->getIsActivated('allowTokenUser', $surveyId)) {
            \reloadAnyResponse\Utilities::setCurrentReloadedWay($surveyId, 'token');
        }

        if (empty($oSurvey) || $oSurvey->tokenanswerspersistence != "Y") {
            return;
        }

        if ($srid == 'new' && $token && $this->getIsActivated('allowTokenUser', $surveyId)) {
            if ($oSurvey->tokenanswerspersistence != "Y" || $oSurvey->showwelcome != 'Y' || $oSurvey->format == 'A') {
                /* Check if create or not … */
                $this->getEvent()->set('response', false);
                return;
            }
        }
        /* if srid is set : all is already done */
        /* need to check maxstep */
        if (!$this->getCurrentSetting('multiAccessTime', $surveyId)) {
            return;
        }

        if (is_null($oResponse)) {
            if (empty($oResponses[0]->submitdate) || $oSurvey->alloweditaftercompletion == 'Y') {
                $oResponse = $oResponses[0];
            }
        }
        if (empty($oResponse)) {
            return;
        }
        if ($since = \reloadAnyResponse\models\surveySession::getIsUsed($surveyId, $oResponse->id, !$this->noactionByUserAgent())) {
            $this->endWithEditionMessage($since);
        }
        if (!$this->noactionByUserAgent()) {
            \reloadAnyResponse\models\surveySession::saveSessionTime($surveyId, $oResponse->id);
        }
    }

    /** @inheritdoc **/
    public function beforeSurveyPage()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        /* Save current session Id to allow same user to reload survey in same browser */
        /* resetAllSessionVariables regenerate session id */
        /* Keep previous session id, if user reload start url it reset the sessionId, need to leav access */
        $surveyid = $this->getEvent()->get('surveyId');
        /* Multiple access to same survey checking */
        $multiAccessTime = $this->getCurrentSetting('multiAccessTime', $surveyid);
        if ($multiAccessTime !== '') {
            Yii::app()->setConfig('surveysessiontime_limit', $multiAccessTime);
        }
        $disableMultiAccess = true;
        if ($multiAccessTime === '0' /* disable by survey */ || $multiAccessTime === '' /* disable globally */) {
            $disableMultiAccess = false;
        }
        if ($disableMultiAccess) {
            $this->checkAccessByUserAgent();
        }

        /* For token : @todo in beforeReloadReponse */
        /* @todo : delete surveySession is save or clearall action */
        if ($disableMultiAccess && ($since = \reloadAnyResponse\models\surveySession::getIsUsed($surveyid, null, !$this->noactionByUserAgent()))) {
            /* This one is done with current session : maybe allow to keep srid in session and reload it ? */
            killSurveySession($surveyid);
            $this->endWithEditionMessage($since);
        }
        /* Clear all action */
        $isClearAll = (App()->request->getPost('clearall') == 'clearall' || App()->request->getPost('move') == 'clearall') && App()->request->getPost('confirm-clearall') == 'confirm';
        if ($isClearAll) {
            $this->actionOnClearAll($surveyid);
            /* If we are there : LS must do it's own action */
            return;
        }
        /* Check POST and current session : throw error if needed */
        if (App()->getRequest()->getPost('reloadAnyResponseSrid')) {
            $currentReloadedSrid = \reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyid);
            $currentSrid = \reloadAnyResponse\Utilities::getCurrentSrid($surveyid);
            \reloadAnyResponse\Utilities::setSaveAutomatic($surveyid);
            if ($currentSrid != App()->getRequest()->getPost('reloadAnyResponseSrid') || ($currentReloadedSrid && $currentReloadedSrid != $currentSrid)) {
                throw new CHttpException(400, $this->translate("Your current session seems invalid with current data."));
            }
            if ($disableMultiAccess && ($since = \reloadAnyResponse\models\surveySession::getIsUsed($surveyid, $currentSrid, !$this->noactionByUserAgent()))) {
                killSurveySession($surveyid);
                $this->endWithEditionMessage($since);
            }
            $token = \reloadAnyResponse\Utilities::getCurrentReloadedToken($surveyid);
            tracevar($this->getCurrentSetting('reloadResetSubmitted', $surveyid));
            \reloadAnyResponse\Utilities::resetLoadedReponse($surveyid, $currentSrid, $token, $this->getCurrentSetting('reloadResetSubmitted', $surveyid));
            $this->surveyId = $surveyid;
            $this->reloadedSrid = $currentSrid;
            return;
        }
        /* Check srid */
        $srid = App()->getRequest()->getQuery('srid');
        if (!$srid) {
            $this->reloadedSrid = \reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyid);
            if ($this->reloadedSrid && is_int($this->reloadedSrid) && $disableMultiAccess && ($since = \reloadAnyResponse\models\surveySession::getIsUsed($currentReloadedSrid, $this->reloadedSrid, !$this->noactionByUserAgent()))) {
                killSurveySession($surveyid);
                $this->endWithEditionMessage($since);
            }
            if ($disableMultiAccess  && !$this->noactionByUserAgent()) {
                /* Always save current srid if needed , only reload can disable this */
                \reloadAnyResponse\models\surveySession::saveSessionTime($surveyid);
            }
            $this->surveyId = $surveyid;
            return;
        }
        $oSurvey = Survey::model()->findByPk($surveyid);
        $token = App()->getRequest()->getParam('token');
        if ($srid == "new" || \reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyid) == "new") {
            $oStarurl = new \reloadAnyResponse\StartUrl($surveyid, $token);
            if ($oStarurl->isAvailable()) {
                $this->reloadedSrid = "new";
                $this->surveyId = $surveyid;
            }
            return;
        }
        if ($disableMultiAccess && ($since = \reloadAnyResponse\models\surveySession::getIsUsed($surveyid, $srid, !$this->noactionByUserAgent()))) {
            killSurveySession($surveyid);
            $this->endWithEditionMessage($since);
        }
        if ($this->loadReponse($surveyid, $srid, App()->getRequest()->getParam('token'), App()->getRequest()->getParam('code'))) {
            $this->surveyId = $surveyid;
            $this->reloadedSrid = $srid;
            $this->reloadedToken = App()->getRequest()->getParam('token');
        }
    }

    /**
     * @see getPluginTwigPath event
     * Do some action when srid is potentially set
     */
    public function getPluginTwigPath()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $this->unsubscribe('getPluginTwigPath');
        if (!$this->surveyId) {
            return;
        }
        \reloadAnyResponse\Utilities::fixToken($this->surveyId);
        $srid = \reloadAnyResponse\Utilities::getCurrentSrid($this->surveyId);
        if ($this->reloadedSrid == "new") {
            if ($srid) {
                \reloadAnyResponse\Utilities::setSaveAutomatic($this->surveyId);
                $this->reloadedSrid = \reloadAnyResponse\Utilities::getCurrentSrid($this->surveyId);
                \reloadAnyResponse\Utilities::setCurrentReloadedSrid($this->surveyId, $srid);
            } else {
                /* What happen ? Unsure on what to do . */
                \reloadAnyResponse\Utilities::setCurrentReloadedSrid($this->surveyId, null);
            }
        }
        if ($srid) {
            if (\reloadAnyResponse\Utilities::getSetting($this->surveyId, 'keepMaxStep')) {
                \reloadAnyResponse\Utilities::setMaxStep($this->surveyId, $srid);
            }
            $ajaxUrl = Yii::app()->getController()->createUrl(
                'plugins/direct',
                array('plugin' => get_class($this), 'function' => 'close','sid' => $this->surveyId,'srid' => $srid)
            );
            $onBeforeUnload = "window.onbeforeunload = function(e) {\n";
            $onBeforeUnload .= "    jQuery.ajax({ async: false, url:'{$ajaxUrl}' });\n";
            $onBeforeUnload .= "}\n";
            Yii::app()->getClientScript()->registerScript("reloadAnyResponseBeforeUnload", $onBeforeUnload, CClientScript::POS_HEAD);
        }
    }

    /**
     * @see getPluginTwigPath event
     * Here : adding path to twig (when needed only)
     */
    public function addPluginTwigPath()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $viewPath = dirname(__FILE__) . "/twig";
        $this->getEvent()->append('add', array($viewPath));
    }

    /**
     * @see beforeQuestionRender event
     * Adding a POST value with current reloaded Srid
     * @return void
     */
    public function beforeQuestionRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->reloadedSrid) {
            $hiddenInput = CHtml::hiddenField('reloadAnyResponseSrid', $this->reloadedSrid);
            $this->getEvent()->set("answers", $this->getEvent()->get("answers") . $hiddenInput);
        }
        $this->reloadedSrid = null;
    }
    /**
     * Delete SurveySession for this event srid
     */
    public function afterSurveyDeleteSurveySession()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $surveyId = $this->getEvent()->get('surveyId');
        $responseId = $this->getEvent()->get('responseId');
        if (!empty($surveyId) && !empty($responseId)) {
            \reloadAnyResponse\models\surveySession::model()->deleteByPk(array('sid' => $surveyId,'srid' => $responseId));
        }
    }

    /** @inheritdoc **/
    public function newDirectRequest()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getEvent()->get('target') != get_class($this)) {
            return;
        }
        $surveyId = Yii::app()->getRequest()->getParam('sid');
        $responseId = Yii::app()->getRequest()->getParam('srid');
        if (\reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyId) != $responseId && \reloadAnyResponse\Utilities::getCurrentSrid($surveyId) != $responseId) {
            \Yii::log(sprintf("Try to reset session for %s in %s", $responseId, $surveyId), \CLogger::LEVEL_INFO, 'plugin.reloadAnyResponse.newDirectRequest');
            return;
        }
        if ($surveyId && $responseId) {
            \reloadAnyResponse\models\surveySession::model()->deleteByPk(array('sid' => $surveyId,'srid' => $responseId));
        }
    }

    /**
     * action on clear all, result are end or return.
     * @param integer $surveyId
     * @return void
     */
    private function actionOnClearAll($surveyId)
    {
        $srid = \reloadAnyResponse\Utilities::getCurrentSrid($surveyId);
        if (empty($srid)) {
            return;
        }
        \reloadAnyResponse\models\surveySession::model()->deleteByPk(array('sid' => $surveyId,'srid' => $srid));

        $reloadedSrid = App()->getRequest()->getPost('reloadAnyResponseSrid');
        if ($srid && $reloadedSrid  && $reloadedSrid != $srid) {
            /* Must throw error : else potential deletion of bad survey */
            /* Lets do the default action */
            return;
        }
        if (empty($reloadedSrid)) {
            $reloadedSrid = \reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyId);
        }
        if (!$reloadedSrid && $this->getCurrentSetting('clearAllActionForced', $surveyId)) {
            /* seems not reloaded : quit */
            return;
        }
        $this->reloadedSrid = $reloadedSrid;
        $this->reloadedToken = \reloadAnyResponse\Utilities::getCurrentReloadedToken($surveyId);
        ;
        /* Disable LS core system : only in case of no delete */
        if ($this->getCurrentSetting('clearAllAction', $surveyId) == 'reset') {
            $_POST['confirm-clearall'] = null;
        }

        $this->unsubscribe('getPluginTwigPath'); /* Other not needed */
        $this->surveyId = $surveyId;
        $this->subscribe('getPluginTwigPath', 'clearAllAction');
    }


    /**
     * Do the clear all action in twig event
     * Thei function is registred in beforeSurveypage
     */
    public function clearAllAction()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $this->unsubscribe('getPluginTwigPath');
        $surveyId = $this->surveyId;
        $action = $this->getCurrentSetting('clearAllAction', $surveyId);
        /* Start by reset the session */
        $currentSrid = $this->reloadedSrid;
        $currentToken = $this->reloadedToken;
        /* what restart url to be used ? */
        /* mimic core, but replace according information */
        $restartUrlParm = array(
            'newtest' => 'Y',
            'lang' => App()->getLanguage(),
        );
        if ($currentToken) {
            $restartUrlParm['token'] = $currentToken;
        }
        $restartComplete = false;
        switch ($action) {
            case 'reset':
                $restartComplete = true;
                break;
            case 'partial':
            default:
                if (SurveyDynamic::model($surveyId)->isCompleted($currentSrid)) {
                    $restartComplete = true;
                    break;
                }
                // no break
            case 'all':
                $oResponse = Response::model($surveyId)->findByPk($currentSrid);
                if (!empty($oResponse)) { /* Can be already deleted by core */
                    if ($oResponse->delete(true)) {
                        if (Survey::model()->findByPk($surveyId)->savetimings == "Y") {
                            SurveyTimingDynamic::model($surveyId)->deleteAll("id=:srid", array(":srid" => $currentSrid)); /* delete timings ( @todo must move it to Response )*/
                        }
                        SavedControl::model()->deleteAll("sid=:sid and srid=:srid", array(":sid" => $surveyId, ":srid" => $currentSrid));
                    }
                }
        }
        killSurveySession($surveyId);
        if ($restartComplete) {
            $StartUrl = new \reloadAnyResponse\StartUrl($surveyId, $currentToken);
            $restartUrl = $StartUrl->getUrl($currentSrid, $restartUrlParm, false);
        }
        if (empty($restartUrl)) {
            $restartUrl = App()->createUrl("survey/index", $restartUrlParm);
        }
        $aSurveyinfo = getSurveyInfo($surveyId, App()->getLanguage());
        $aSurveyinfo['surveyUrl'] = $restartUrl;
        $aSurveyinfo['newUrl'] = $restartComplete ? null : $restartUrl;
        $aSurveyinfo['restartUrl'] = $restartComplete ? $restartUrl : null;
        $aSurveyinfo['include_content'] = 'deleted';
        if ($restartComplete) {
            $aSurveyinfo['include_content'] = 'clearall';
        }
        $this->subscribe('getPluginTwigPath', 'addPluginTwigPath');
        Yii::app()->twigRenderer->renderTemplateFromFile(
            "layout_global.twig",
            array(
                'oSurvey' => Survey::model()->findByPk($surveyId),
                'aSurveyInfo' => $aSurveyinfo
            ),
            false
        );
    }

    /**
    * Get boolean value for setting activation
    * @param string existing $setting
    * @param integer $surveyid
    * @return boolean
    */
    private function getIsActivated($setting, $surveyid)
    {
        $activation = $this->get($setting, 'Survey', $surveyid, "");
        if ($activation === '') {
            $activation = $this->get($setting, null, null, $this->settings[$setting]['default']);
        }
        return (bool) $activation;
    }

    /**
    * Create needed DB
    * @return void
    */
    private function createDb()
    {
        if ($this->get("dbVersion") == self::$dbVersion) {
            return;
        }
        /* dbVersion not needed */
        if (!$this->api->tableExists($this, 'responseLink') && !$this->api->tableExists($this, 'surveySession')) {
            $this->api->createTable($this, 'responseLink', array(
                'sid' => 'int not NULL',
                'srid' => 'int not NULL',
                'token' => 'text',
                'accesscode' => 'text',
            ));
            $this->api->createTable($this, 'surveySession', array(
                'sid' => 'int not NULL',
                'srid' => 'int not NULL',
                'token' => 'string(55)',
                'session' => 'text',
                'lastaction' => 'datetime'
            ));
            $this->set("dbVersion", 2);
        }

        if (!$this->get("dbVersion") || $this->get("dbVersion") < 2) {
            $tableName = $this->api->getTable($this, 'surveySession')->tableName();
            Yii::app()->getDb()->createCommand()->alterColumn($tableName, 'sid', 'int not NULL');
            Yii::app()->getDb()->createCommand()->alterColumn($tableName, 'srid', 'int not NULL');
            $tableSchema = App()->getDb()->getSchema()->getTable($tableName);
            $tableName = $this->api->getTable($this, 'responseLink')->tableName();
            Yii::app()->getDb()->createCommand()->alterColumn($tableName, 'sid', 'int not NULL');
            Yii::app()->getDb()->createCommand()->alterColumn($tableName, 'srid', 'int not NULL');
            $tableSchema = App()->getDb()->getSchema()->getTable($tableName);
            $this->set("dbVersion", 3);
        }

        if ($this->get("dbVersion") < 3) {
            $tableName = $this->api->getTable($this, 'surveySession')->tableName();
            if (empty(App()->getDb()->getSchema()->getTable($tableName)->primaryKey)) {
                Yii::app()->getDb()->createCommand()->addPrimaryKey('surveysession_sidsrid', $tableName, 'sid,srid');
            }
            $tableName = $this->api->getTable($this, 'responseLink')->tableName();
            if (empty(App()->getDb()->getSchema()->getTable($tableName)->primaryKey)) {
                Yii::app()->getDb()->createCommand()->addPrimaryKey('responselink_sidsrid', $tableName, 'sid,srid');
            }
            $this->set("dbVersion", 3);
        }
        if ($this->get("dbVersion") < 5) {
            if (!$this->api->tableExists($this, 'responseMaxstep')) {
                $this->api->createTable($this, 'responseMaxstep', array(
                    'sid' => 'int not NULL',
                    'srid' => 'int not NULL',
                    'maxstep' => 'int not NULL',
                ));
                $tableName = $this->api->getTable($this, 'responseMaxstep')->tableName();
                App()->getDb()->createCommand()->addPrimaryKey('responsemaxstep_sidsrid', $tableName, 'sid,srid');
            }
        }
        /* all done */
        $this->set("dbVersion", self::$dbVersion);
    }

    /**
     * Add this translation just after loaded all plugins
     * @see event afterPluginLoad
     */
    public function setConfigInAfterPluginLoad()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $this->createDb();
        if (!empty(Yii::app()->getConfig('surveysessiontime_limit'))) {
            /* Allow to force surveysessiontime_limit in config.php , to do : show it to admin */
            Yii::app()->setConfig('surveysessiontimeDisable', true);
        }
        if (empty(Yii::app()->getConfig('surveysessiontime_limit'))) {
            Yii::app()->setConfig('surveysessiontime_limit', $this->get('multiAccessTime', null, null, $this->settings['multiAccessTime']['default']));
        }
        $messageSource = array(
            'class' => 'CGettextMessageSource',
            'cachingDuration' => 3600, // 1 hour, Must reset only when needed
            'useMoFile' => true,
            'basePath' => __DIR__ . DIRECTORY_SEPARATOR . 'locale',
            'catalog' => 'messages',// default from Yii
        );
        Yii::app()->setConfig('reloadAnyResponseApi', \reloadAnyResponse\Utilities::API);
        Yii::app()->setComponent('ReloadAnyResponseMessages', $messageSource);
    }

    /**
    * Create Survey and add current response in $_SESSION
    * @param integer $surveyid
    * @param integer $srid
    * @param string $token
    * @param string $accesscode
    * @throws Error
    * @return void|true
    */
    private function loadReponse($surveyid, $srid, $token = null, $accesscode = null)
    {
        /* Utilities check access */
        return \reloadAnyResponse\Utilities::loadReponse($surveyid, $srid, $token, $accesscode);
    }

    /**
     * Create a new response for token
     * @todo : validate if we need it or if it's a bug in LS version tested
     * @param int $surveyid
     * @param string $token
     * @return null|\Response
     */
    private function createNewResponse($surveyid, $token)
    {
        $oSurvey = Survey::model()->findByPk($surveyid);
        if ($this->accessibleWithToken($oSurvey)) {
            return;
        }
        if ($oSurvey->tokenanswerspersistence != "Y") {
            return;
        }
        //~ if($oSurvey->alloweditaftercompletion != "Y") {
        //~ return;
        //~ }
        /* some control */
        if (
            !(
                ($this->getIsActivated('allowAdminUser', $surveyid) && Permission::model()->hasSurveyPermission($surveyid, 'response', 'create'))
                ||
                ($this->getIsActivated('allowTokenUser', $surveyid))
            )
        ) {
            // Disable here
            $this->log("Try to create a new reponse with token but without valid rights", 'warning');
            return;
        }
        $oToken = Token::model($surveyid)->findByAttributes(array('token' => $token));
        if (empty($oToken)) {
            return;
        }
        $oResponse = Response::create($surveyid);
        $oResponse->token = $oToken->token;
        $oResponse->startlanguage = Yii::app()->getLanguage();
        /* @todo generate if not set */
        if (version_compare(Yii::app()->getConfig('versionnumber'), "3", ">=")) {
            $oResponse->seed = isset($_SESSION['survey_' . $surveyid]['startingValues']['seed']) ? $_SESSION['survey_' . $surveyid]['startingValues']['seed'] : null;
        }
        $oResponse->lastpage = -1;
        if ($oSurvey->datestamp == 'Y') {
            $date = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", Yii::app()->getConfig('timeadjust'));
            $oResponse->datestamp = $date;
            $oResponse->startdate = $date;
        }
        $oResponse->save();
        return $oResponse;
    }

    /**
     * Did this survey have token with reload available
     * @var \Survey
     * @return boolean
     */
    private function accessibleWithToken($oSurvey)
    {
        Yii::import('application.helpers.common_helper', true);
        return $oSurvey->anonymized != "Y" && tableExists("{{tokens_" . $oSurvey->sid . "}}");
    }

    /**
     * Ending with the delay if renderMessage is available (if not : log as error …)
     * @param float $since last edit
     * @return void
     */
    private function endWithEditionMessage($since)
    {
        $messageString = sprintf($this->translate("Sorry, someone update this response to the questionnaire a short time ago. The last action was made less than %s minutes ago."), ceil($since));
        self::ThrowHttpException("409", $messageString);
    }

    /**
     * Check access according to HTTP_USER_AGENT
     * @throw Exception
     * @return void
     */
    private function checkAccessByUserAgent()
    {
        $noHttpUserAgent = $this->get('noHttpUserAgent', null, null, $this->settings['noHttpUserAgent']['default']);
        if (empty($_SERVER['HTTP_USER_AGENT'])) {
            if ($noHttpUserAgent == 'forbidden') {
                self::ThrowHttpException(403, "No User Agent");
            }
            // No action to do : can return
            return;
        }
        $noBotHttpUserAgent = $this->get('botHttpUserAgent', null, null, $this->settings['botHttpUserAgent']['default']);
        if ($noBotHttpUserAgent == 'forbidden' && $this->isBotByRegexp($_SERVER['HTTP_USER_AGENT'])) {
            self::ThrowHttpException(403, "Invalid header sent : Bot header");
        }
    }

    /**
     * Check if need action (disable access time) by user aganet
     * @return boolean
     */
    private function noactionByUserAgent()
    {
        $noHttpUserAgent = $this->get('noHttpUserAgent', null, null, $this->settings['noHttpUserAgent']['default']);
        if (empty($_SERVER['HTTP_USER_AGENT'])) {
            if ($noHttpUserAgent == '') {
                return true;
            }
            return false;
        }
        $noBotHttpUserAgent = $this->get('botHttpUserAgent', null, null, $this->settings['botHttpUserAgent']['default']);
        if ($noBotHttpUserAgent == '' && $this->isBotByRegexp($_SERVER['HTTP_USER_AGENT'])) {
            return true;
        }
        return false;
    }

    /**
     * Check access according to HTTP_USER_AGENT
     * @throw Exception
     * @return boolean
     */
    private function isBotByRegexp($useragent)
    {
        $botRegexp = $this->get('botRegexp', null, null, $this->settings['botRegexp']['default']);
        if ($botRegexp === '') {
            $botRegexp = '/bot|crawl|slurp|spider|mediapartners|lua-resty-http/i';
        }
        if (!trim($botRegexp)) {
            return false;
        }
        return preg_match($botRegexp, $useragent);
    }
    /**
     * Get current setting for current survey (use empty string as null value)
     * @param string setting to get
     * @param integer survey id
     * @return string|array|null
     */
    private function getCurrentSetting($setting, $surveyId = null)
    {
        if ($surveyId) {
            $value = $this->get($setting, 'Survey', $surveyId, '');
            if ($value !== '') {
                return $value;
            }
        }
        $default = (isset($this->settings[$setting]['default'])) ? $this->settings[$setting]['default'] : null;
        return $this->get($setting, null, null, $default);
    }

    /**
     * get translation
     * @param string
     * @return string
     */
    private function translate($string, $language = null)
    {
        $messageSource = get_class($this) . 'Messages';
        return Yii::t('', $string, array(), $messageSource);
    }

    /**
     * Throw specific error
     * @param @errorCode (404/401/403 totally supported)
     * @param @errorMessage
     * @param $surveyId
     * @throw CHttpException
     */
    private static function ThrowHttpException($errorCode, $errorMessage, $surveyId = null)
    {
        $errorCodeHeader = array(
            '401' => "401 Unauthorized",
            '403' => "403 Forbidden",
            '404' => "404 Not Found",
            '409' => "409 Conflict",
        );

        $limesurveyVersion = Yii::app()->getConfig("versionnumber");
        if (version_compare($limesurveyVersion, "3.14.0", '>=')) {
            // @todo : Set template by survey
            throw new CHttpException($errorCode, $errorMessage);
        }

        if (!array_key_exists($errorCode, $errorCodeHeader)) {
            // Unable to do own system
            throw new CHttpException($errorCode, $errorMessage);
        }
        if (version_compare($limesurveyVersion, "3.0.0", '>=')) {
            header($_SERVER["SERVER_PROTOCOL"] . " " . $errorCodeHeader[$errorCode], true, $errorCode);
            Yii::app()->twigRenderer->renderTemplateFromFile(
                "layout_errors.twig",
                array('aSurveyInfo' => array(
                    'aError' => array(
                        'error' => $errorCodeHeader[$errorCode],
                        'title' => $errorCodeHeader[$errorCode],
                        'message' => $errorMessage,
                    ),
                    'adminemail' => null,
                    'adminname' => Yii::app()->getConfig('siteadminname'),
                )),
                false
            );
            App()->end();
        }
        /* lesser than 3 */
        if (Yii::getPathOfAlias('renderMessage')) {
            $message = CHtml::tag("h1", array("class" => 'text-danger'), $errorCodeHeader[$errorCode]);
            $message .= CHtml::tag("div", array("class" => 'alert alert-danger'), $errorMessage);
            header($_SERVER["SERVER_PROTOCOL"] . " " . $errorCodeHeader[$errorCode], true, $errorCode);
            \renderMessage\messageHelper::renderContent($message);
            App()->end();
        }
        throw new CHttpException($errorCode, $errorMessage);
    }

    /**
     * Set language according to survey or current language
     * @param $surveyid
     * @return $void
     */
    private function fixLanguage($surveyId = null)
    {
        $currentLang = Yii::app()->getLanguage();
        if ((empty($currentLang) || $currentLang == "en_US")) {
            if (!empty($_SESSION['adminlang']) && $_SESSION['adminlang'] != "auto") {
                $currentLang = $_SESSION['adminlang'];
            } else {
                $currentLang = Yii::app()->getConfig("defaultlang");
            }
        }
        if ($surveyId) {
            $oSurvey = Survey::model()->findByPk($surveyId);
            if ($oSurvey && !in_array($currentLang, $oSurvey->getAllLanguages())) {
                $currentLang = $oSurvey->language;
            }
        }
        Yii::app()->setLanguage($currentLang);
    }

    /**
     * Get the help about token user state
     * @param Survey $oSurvey
     * @return string
     */
    public function getTokenUserhelp($oSurvey)
    {
        $allowTokenUserHelp = "<ul>";
        $allowTokenUserHelp .= "<li class='text-info'>" . $this->translate("If you use another plugin, like questionExtraSurvey : you don't need to set it here.") . "</li>";
        if ($oSurvey->getIsAnonymized()) {
            $allowTokenUserHelp .= "<li class='text-danger'>" . $this->translate("Token user can not edit response because survey is anonymous.") . "</li>";
        } else {
            $allowTokenUserHelp .= "<li class='text-success'>" . $this->translate("Token user can edit response (if plugin allow it).") . "</li>";
        }
        /* The default action */
        if (!$oSurvey->getIsAnonymized()) {
            if($oSurvey->isAllowEditAfterCompletion && $oSurvey->isTokenAnswersPersistence) {
                $allowTokenUserHelp .= "<li class='text-info'>" . $this->translate("With token : default action is to reload last response submitted or not.") . "</li>";
            }
            if($oSurvey->isAllowEditAfterCompletion && !$oSurvey->isTokenAnswersPersistence) {
                $allowTokenUserHelp .= "<li class='text-info'>" . $this->translate("With token : default action is to create a new response, it's the best solution to allow srid='new'.") . "</li>";
            }
            if(!$oSurvey->isAllowEditAfterCompletion) {
                $allowTokenUserHelp .= "<li class='text-warning'>" . $this->translate("With token : User can not create new reponse : you must allow edit after completion for this.") . "</li>";
            }
            
        }
        $allowTokenUserHelp .= "</ul>";
        return $allowTokenUserHelp;
    }
    /**
    * @inheritdoc adding string, by default current event
    * @param string
    */
    public function log($message, $level = \CLogger::LEVEL_TRACE, $logDetail = null)
    {
        if (!$logDetail && $this->getEvent()) {
            $logDetail = $this->getEvent()->getEventName();
        } // What to put if no event ?
        parent::log($message, $level);
        Yii::log($message, $level, 'application.plugins.reloadAnyResponse.' . $logDetail);
    }
}
